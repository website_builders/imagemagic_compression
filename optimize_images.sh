#!/bin/bash

mkdir "compressed"

cp ./*.jpg ./compressed

# sudo mogrify -verbose -path ./compressed -filter Triangle -define filter:support=2 -unsharp 0.25x0.25+8+0.065 -dither None -posterize 136 -quality 82 -define jpeg:fancy-upsampling=off -define png:compression-filter=5 -define png:compression-level=9 -define png:compression-strategy=1 -define png:exclude-chunk=all -interlace none -colorspace sRGB -strip *.jpg
# sudo mogrify -verbose -path ./compressed -filter Triangle -define filter:support=2 -dither None -quality 80 -define jpeg:fancy-upsampling=off -define png:compression-filter=5 -define png:compression-level=9 -define png:compression-strategy=1 -define png:exclude-chunk=all -interlace none -colorspace sRGB -strip *.jpg
# sudo mogrify -verbose -path ./compressed -strip -interlace Plane -quality 80% *.jpg
mogrify -verbose -path ./compressed -strip -quality 90 *.jpg
